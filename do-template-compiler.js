const _do = require('./utils/do')
const parse5 = require('parse5')
const treewalker = require('./utils/treewalker.js')

const doTemplate = {
  markup: ({ content }) => {
    const parsedContent = parse5.parseFragment(content)
    treewalker(parsedContent, {
      pre (node, parent, fileImports, localContext) {
        if (node.nodeName === 'do-filesource') {
          _do.preDoFileSource(node, parent, localContext)
        } else if (node.nodeName === 'do-import') {
          _do.preDoImport(node, parent, fileImports, localContext)
        } else if (node.nodeName === 'do-insert') {
          _do.preDoInsert(node, parent, fileImports, localContext)
        } else if (node.nodeName === 'do-set') {
          _do.preDoSet(node, parent, localContext)
        } else if (node.nodeName === 'do-replace') {
          _do.preDoReplace(node, parent, localContext)
        } else if (node.nodeName === 'do-manipulate') {
          _do.preDoManipulate(node, parent, localContext)
        }
      },
      post (node, parent, fileImports, localContext) {
        if (node.nodeName === 'do-insert') {
          _do.postDoInsert(node, parent, localContext)
        }
      }
    })
    const serializedContent = parse5.serialize(parsedContent)
    return { code: serializedContent }
  }
}

module.exports = doTemplate
