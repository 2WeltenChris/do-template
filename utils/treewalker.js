// Treewalker with pre und post manipulation
// also we need the context which will be saved in the variable super

const traverse = (root, options = {}) => {
  const { pre, post } = options

  const fileImports = {}
  // localContext.super is reserved for data which must be transfered forward and/or backwards in the tree
  const localContext = {
    super: []
  }

  const visit = (node, parent) => {
    let res

    if (!node) {
      return
    }

    if (pre) {
      res = pre(node, parent, fileImports, localContext)
    }

    const { childNodes } = node
    if (res !== false && (Array.isArray(childNodes) && childNodes.length >= 0)) {
      for (let i = 0, len = childNodes.length; i < len; i++) {
        if (childNodes.reduceIndex) {
          delete childNodes.reduceIndex
          i -= 1
        }
        visit(childNodes[i], node)
      }
    }

    if (post) {
      post(node, parent, fileImports, localContext)
    }

    // if this node shall be deleted, we must assure, that the index will be corrected
    if (node.deleteNode) {
      const indexInParent = parent.childNodes.indexOf(node)
      parent.childNodes.splice(indexInParent, 1)
      parent.childNodes.reduceIndex = true
    }
    if (node.nodeName === 'do-import') {
      //
    }
  }

  visit(root, null)
}

module.exports = traverse
