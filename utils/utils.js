const { parse5Adapter } = require('css-select-parse5-adapter')
const CssSelect = require('css-select')
const { readFileSync } = require('fs')
const { resolve } = require('path')

const regExComment = /\/\*[\s\S]*?\*\/|\/\/.*/

const addToArray = (array, i, element) => {
  array.splice(i, 0, element)
}

const markNodeToDelete = (array, i) => {
  function nodeIsEmpty (i) {
    return (array[i] && array[i].value && array[i].value.trim() === '')
  }
  if (array.length > 0) {
    array[i].deleteNode = true
    // we mark the prev and next element to delete, if it is empty or a break (\n)
    if (nodeIsEmpty(i + 1)) array[i + 1].deleteNode = true
    if (i > 0 && nodeIsEmpty(i - 1)) array[i - 1].deleteNode = true
  }
}

const getAttributes = (node) => {
  const attributes = {}
  node.attrs.forEach((attr) => {
    attributes[attr.name] = attr.value
  })
  return attributes
}

const getMainBranch = (node) => {
  let mainBranch = node
  while (mainBranch.parentNode) {
    mainBranch = mainBranch.parentNode
  }
  return mainBranch
}

// adding functions from imported (and used) extern files
const importContextFunctions = (scriptNode, file, name) => {
  // remove comments, then check if do_import or do_execute + name still exists
  scriptNode.childNodes[0].value.replace(regExComment, '')
  const searchParam = new RegExp('[^\\w](do_import\\((\'|")' + name + '(\'|")\\))|[^\\w](do_execute\\((\'|")' + name + '(\'|")\\))', 'g')
  const foundDoImport = scriptNode.childNodes[0].value.match(searchParam)

  if (foundDoImport) {
    const scriptToImport = CssSelect.selectAll('script', file, { adapter: parse5Adapter })[0]
    if (scriptToImport && scriptToImport.childNodes.length > 0) {
      scriptToImport.childNodes.forEach((child) => {
        // do_context.set(x) must be stated before being called. Therefore we place it at the top of the script.
        scriptNode.childNodes[0].value = `_do_context.set('${name}', \`${child.value.toString()}\`)\n${scriptNode.childNodes[0].value}`
      })
    } else {
      console.warn('No script to import found in ' + name)
    }
  }
}

// check if script context="module" exists if not, create else add standard do-function block
const initScriptContextModule = (node, fileImport, name) => {
  const mainBranch = getMainBranch(node)

  const scriptTags = CssSelect.selectAll('script', mainBranch, { adapter: parse5Adapter })
  if (scriptTags && scriptTags.length > 0) {
    let moduleContextScriptNode
    let scriptNode
    scriptTags.forEach((tag) => {
      if (tag.attrs.find(a => a.name === 'context' && a.value === 'module')) {
        moduleContextScriptNode = tag
      } else {
        scriptNode = tag
      }
    })

    // no 'standard' script, no insert neccessary
    if (!scriptNode) return
    importContextFunctions(scriptNode, fileImport, name)
    // check if init was already made
    if (mainBranch.scriptContextModuleInit) {
      return
    }
    let scriptModuleChild = {
      nodeName: '#text',
      value: `let _do_context = new Map();
      async function do_import (key, callback) {
      let module = await import('data:text/javascript;base64,' + btoa(_do_context.get(key)));
      if (callback) callback(module);
      return module;
      }
      async function do_execute (key) {
      new Function(_do_context.get(key))()
      }`
    }

    if (moduleContextScriptNode) {
      scriptModuleChild.parentNode = moduleContextScriptNode
      moduleContextScriptNode.childNodes.push(scriptModuleChild)
    } else {
      const newScriptModule = {
        attrs: [{
          name: 'context',
          value: 'module'
        }],
        childNodes: [scriptModuleChild],
        namespaceURI: 'http://www.w3.org/1999/xhtml',
        nodeName: 'script',
        parentNode: mainBranch,
        tagName: 'script'
      }

      scriptModuleChild = {
        parentNode: newScriptModule
      }
      newScriptModule.childNodes.push(scriptModuleChild)
      mainBranch.childNodes.push(newScriptModule)
    }
    mainBranch.scriptContextModuleInit = true
  }
}

const doInsertCss = (node, fileImports, name) => {
  const mainBranch = getMainBranch(node)
  const styleTag = CssSelect.selectAll('style', mainBranch, { adapter: parse5Adapter })[0]
  if (styleTag) {
    styleTag.childNodes.forEach((child) => {
      // we remove all comments (yes blame me) and check then if we still have @do-inserts
      // const searchWithComment = new RegExp(regExStartJsComment + '(@do-insert "' + name + '";?)' + regExEndJsComment, 'g')
      child.value = child.value.replace(regExComment, '')
      const searchParam = new RegExp('(@do-insert\\s*"' + name + '";?)', 'g')
      const foundInserts = styleTag.childNodes[0].value.match(searchParam)
      if (foundInserts && foundInserts.length > 0) {
        child.value = child.value.replace(searchParam, '')
        const importCss = CssSelect.selectAll('style', fileImports[name], { adapter: parse5Adapter })[0]
        if (importCss) {
          importCss.childNodes.forEach((child) => {
            styleTag.childNodes.push(child)
          })
        }
      }
    })
  }
}

const loadFileForPreprocessing = (filePath) => {
  let file = readFileSync(filePath, 'utf8')
  file = '<do-filesource>' + resolve(filePath, '../') + '</do-filesource>' + file
  return file
}

module.exports = {
  getAttributes,
  addToArray,
  markNodeToDelete,
  initScriptContextModule,
  doInsertCss,
  loadFileForPreprocessing
}
