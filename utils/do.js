const fs = require('fs')
const path = require('path')
const parse5 = require('parse5')
const { parse5Adapter } = require('css-select-parse5-adapter')
const CssSelect = require('css-select')
const { cloneDeep } = require('lodash/lang')
const utils = require('./utils')

const preDoFileSource = (node, parent, localContext) => {
  localContext.svelteFileLocation = node.childNodes[0].value
  const indexInParent = parent.childNodes.indexOf(node)
  if (indexInParent !== -1) {
    utils.markNodeToDelete(parent.childNodes, indexInParent)
  }
}

const preDoImport = (node, parent, fileImports, localContext) => {
  const attributes = {}
  node.attrs.forEach((attr) => {
    attributes[attr.name] = attr.value
  })
  if (attributes.src && attributes.as) {
    let directory = attributes.src
    if (attributes.src.startsWith('.')) {
      directory = path.resolve(localContext.svelteFileLocation, attributes.src)
    }
    fileImports[attributes.as] = cloneDeep(parse5.parse(fs.readFileSync(directory, 'utf8')))
    // script adding
    // TODO: insert script if needed? (css should be filtered later by postCSS)
    // addToMainBranch('style', node, fileImports[attributes.as])
    // addToMainBranch('script', node, fileImports[attributes.as])
  } else {
    throw new Error('do-import has no "src" and "as"')
  }
  // check style if it has @do-import "name";
  utils.doInsertCss(node, fileImports, attributes.as)

  utils.initScriptContextModule(node, fileImports[attributes.as], attributes.as)
  // remove do-import from parent node
  const indexInParent = parent.childNodes.indexOf(node)
  if (indexInParent !== -1) {
    utils.markNodeToDelete(parent.childNodes, indexInParent)
  }
}

// preDoInsert will only put a component in localContext if it has select and from.
// the insert itself will be made by postDoInsert because there might be changes by other <do-x>
const preDoInsert = (node, parent, fileImports, localContext) => {
  const attributes = utils.getAttributes(node)
  // check select and from (i.e. select="#card" from="design")
  if (attributes.select && attributes.from) {
    if (fileImports[attributes.from]) {
      const foundComponent = CssSelect.selectAll(attributes.select, fileImports[attributes.from], { adapter: parse5Adapter })
      if (foundComponent.length < 1) {
        console.warn(`No component ${attributes.from} found!`)
      } else if (foundComponent.length > 1) {
        console.warn(`More than 1 component for ${attributes.from} was found! We use the first result!`)
      }
      // localContext.super must stay an Array for cssSelect
      localContext.super = [cloneDeep(foundComponent[0])]
    } else {
      throw new Error(`Missing do-import for ${attributes.from}!`)
    }
    // now check if <script module="context"> exists and is needed and has the standard function
    utils.initScriptContextModule(node, fileImports[attributes.from], attributes.from)
  }
}

// remove <do-insert> and insert the found component
const postDoInsert = (node, parent, localContext) => {
  const attributes = utils.getAttributes(node)
  if (attributes.select && attributes.from) {
    const indexInParent = parent.childNodes.indexOf(node)
    utils.markNodeToDelete(parent.childNodes, indexInParent)
    utils.addToArray(parent.childNodes, indexInParent, localContext.super[0])
    localContext.super = []
  } else if (attributes.reference) {
    const components = localContext[attributes.reference]
    if (!components) {
      throw new Error(`Missing import or "select" "as" for ${attributes.reference}!`)
    }
    const indexInParent = parent.childNodes.indexOf(node)
    utils.markNodeToDelete(parent.childNodes, indexInParent)
    utils.addToArray(parent.childNodes, indexInParent, cloneDeep(components[0]))
    // if copied element has a delete token, we have to remove it.
    if (parent.childNodes[indexInParent].deleteNode) {
      delete parent.childNodes[indexInParent].deleteNode
    }
  }
}

const preDoSet = (node, parent, localContext) => {
  // doSet doesn't know what element should be changed. This must be handled by the parent!
  const parentAttributes = utils.getAttributes(parent)
  let elementsToChange
  if (parentAttributes.reference) {
    elementsToChange = localContext[parentAttributes.reference]
    if (!elementsToChange) {
      throw new Error(`Missing import or "select" "as" for ${parentAttributes.reference}!`)
    }
  } else if (parent.nodeName === 'do-manipulate') {
    elementsToChange = parent.manipulateThis
  }

  elementsToChange.forEach((element) => {
    // change or add attribute
    node.attrs.forEach((attribute) => {
      const existingAttribute = element.attrs.findIndex(attr => attr.name === attribute.name)
      if (existingAttribute !== -1) {
        element.attrs[existingAttribute] = attribute
      } else {
        element.attrs.push(attribute)
      }
      // if we use svelte attributes and there are already standard html attributes, we remove those
      // i.e. onclick="myFunction" and on:click={myFunction}
      if (attribute.name.includes(':')) {
        const existingAttrInHtmlStyle = element.attrs.findIndex(attr => attr.name === attribute.name.replace(':', '').toLowerCase())
        if (existingAttrInHtmlStyle !== -1) {
          element.attrs.splice(existingAttrInHtmlStyle, 1)
        }
      }
    })
  })
  // remove <do-set>
  const indexInParent = parent.childNodes.indexOf(node)
  utils.markNodeToDelete(parent.childNodes, indexInParent)
}

const preDoReplace = (node, parent, localContext) => {
  const attributes = utils.getAttributes(node)
  if (attributes.find) {
    const foundComponents = CssSelect.selectAll(attributes.find, localContext.super, { adapter: parse5Adapter })
    foundComponents[0].childNodes[0].value = node.childNodes[0].value
  } else if (attributes.select || attributes.as) {
    // TODO: mit cg klären, wegen Variable zurückschreiben
    //
    //
    if (!attributes.select && !attributes.as) {
      throw new Error('do-replace attribute "select" or "as" is missing!')
    }

    if (localContext[attributes.as]) {
      console.warn(`Variable "${attributes.as} is already defined!`)
    }
    localContext[attributes.as] = CssSelect.selectAll(attributes.select, localContext.super, { adapter: parse5Adapter })
    localContext[attributes.as].originalCssClass = attributes.select
    localContext[attributes.as].forEach((c) => {
      const indexInParent = c.parentNode.childNodes.indexOf(c)
      utils.markNodeToDelete(c.parentNode.childNodes, indexInParent)
      node.childNodes.forEach((node, i) => {
        utils.addToArray(c.parentNode.childNodes, indexInParent + i, node)
      })
    })
  } else {
    throw new Error('do-replace has no or unknown attributes!')
  }
}

// just search for the elements to manipulate. The manipulation will be made in <do-set>
const preDoManipulate = (node, parent, localContext) => {
  const attributes = utils.getAttributes(node)
  const parentAttributes = utils.getAttributes(parent)
  // should be localContext.super[0] but to get sure...
  const contextElement = CssSelect.selectAll(parentAttributes.select, localContext.super, { adapter: parse5Adapter })
  const foundElements = CssSelect.selectAll(attributes.select, contextElement, { adapter: parse5Adapter })
  if (foundElements.length === 0) {
    throw new Error(`do-manipulate can't find the element ${attributes.select}`)
  }
  node.manipulateThis = foundElements
  const indexInParent = parent.childNodes.indexOf(node)
  utils.markNodeToDelete(parent.childNodes, indexInParent)
}

module.exports = {
  preDoFileSource,
  preDoImport,
  preDoInsert,
  preDoManipulate,
  preDoReplace,
  preDoSet,
  postDoInsert
}
