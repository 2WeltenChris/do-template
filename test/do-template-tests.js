// whitelist variable for standardJS
/* global describe, it */

const svelte = require('svelte/compiler')
const assert = require('assert')
const examples = require('./html/htmlExamples.js')
const doTemplate = require('../do-template-compiler.js')
const { resolve } = require('path')
const { loadFileForPreprocessing } = require('../utils/utils')

describe('preprocessing', () => {
  describe('get data', () => {
    it('gets the html code as a string', () => {
      svelte.preprocess(examples.doExampleComplete, {
        markup: ({ content }) => {
          assert.deepStrictEqual(content.includes('<script>'), true)
          assert.deepStrictEqual(content.includes('from="design"'), true)
        }
      })
    })

    it('returns the html string without changing', async () => {
      const { code } = await svelte.preprocess(examples.doExampleComplete, {
        markup: ({ content }) => {
          return { code: content }
        }
      })
      assert.deepStrictEqual(true, code.includes('<do-import'))
    })

    it('should work with a html file', async () => {
      const file = loadFileForPreprocessing(resolve(__dirname, './html/completeExample.html'))
      const { code } = await svelte.preprocess(file, doTemplate)
      assert.deepStrictEqual(code.includes('<div id="card">'), true)
      // console.log(code)
    })

    it('should work with a svelte file and complete html files', async () => {
      const file = loadFileForPreprocessing(resolve(__dirname, './svelte/svelteExample.svelte'))
      const { code } = await svelte.preprocess(file, doTemplate)
      assert.deepStrictEqual(code.includes('_do_context.set('), true)
      console.log(code)
    })
  })
})
