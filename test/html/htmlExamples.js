const simpleHtmlPart = '<h1>Hello World</h1>'
// No blank / break directly after '`'
const simpleHtml = `<!DOCTYPE html><script>
    let myTitle = "Hello world!" 
    let myotherVar = "x"
  </script>
  <style>
    h1 { color: red; }
  </style>
  <h1>{myTitle}</h1>`

const importExample = `<script>
let myTitle = "Hello world!"
</script>
<style>
  h1 { color: red; }
</style> 
<do-import src="../test/htmlExample/design_sample.html" as="design"></do-import>
<div>i stand behind do-import</div>`

const simpleInsertExample = `<script>
import  MyInput from "./MyInput.svelte"
export let okAction
let title = "Super Card"
</script>
<style>
  h1 { color: red; }
</style> 
<do-import src="../test/htmlExample/design_sample.html" as="design"></do-import>
<div id="wrapper">
  <do-insert select="#card" from="design">
    <do-replace find="#card .title">{title}</do-replace>
    <do-replace select="input" as="source">
      <MyInput placeholder={source.placeholder}>{source.name}</MyInput>
    </do-replace>
  </do-insert>
  <div>I stand behind do-import</div>
</div>`

const insertAndReplaceExample = `<do-import src="../test/htmlExample/design_sample.html" as="design"></do-import>
  <do-insert select="#card" from="design">
    <do-replace find="#card .title">{title}</do-replace>
    <do-replace select="input" as="source">
      <MyInput placeholder={source.placeholder}>{source.name}</MyInput>
    </do-replace>
  </do-insert>
  <div>I stand behind do-import</div>`

const setExample = `<do-import src="../test/htmlExample/design_sample.html" as="design"></do-import>
  <do-insert select="#card" from="design">
    <do-replace select="#okButton" as="okButton">
      {#if okAction}
        <do-insert reference="okButton"><do-set on:click="{() => okAction}"></do-set></do-insert>
      {/if}
    </do-replace>
  </do-insert>
  <div>I stand behind do-import</div>`

// No blank / break directly after '`'
const doExampleComplete = `<script>
  import  MyInput from "./MyInput.svelte"
  export let okAction
  let title = "Super Card"
  do_import('design')
  do_execute('footer')
  </script>
  <script context="module">
    let myVar = "this is my html module script"
  </script>
  <style>
    h1 { color: red; }
    h2 { color: green;}
    @do-insert "design";
  </style> 
  <do-import src="../test/htmlExample/design_sample.html" as="design"></do-import>
  <do-import src="../test/htmlExample/design_sample_2.html" as="footer"></do-import>
  <div id="wrapper">
    <do-insert select="#card" from="design">
      <do-replace find="#card .title">{title}</do-replace>
      <do-replace select="input" as="source">
        <MyInput placeholder={source.placeholder}>{source.name}</MyInput>
      </do-replace>
      <do-replace select="#okButton" as="okButton">
        {#if okAction}
          <do-insert reference="okButton">
            <do-set on:click="{() => okAction}"></do-set>
          </do-insert>
        {/if}
      </do-replace>
      <do-manipulate select="#okButton">
        <do-set class="{classes} newClass"></do-set>
        <do-set style="background-color: green;"></do-set>
      </do-manipulate>
    </do-insert>
  </div>
  <div> something between card and footer </div>
  <do-insert select="#footer" from="footer" /></do-insert>`

module.exports = {
  simpleHtmlPart,
  simpleHtml,
  importExample,
  simpleInsertExample,
  insertAndReplaceExample,
  setExample,
  doExampleComplete
}
