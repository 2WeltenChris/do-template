# do-template

This is a preprocessing-Plugin for svelte, which allows you to use a (from us defined) template-language which will make svelte-component from plain-html-files.

## Usage

Must be used in the svelte preprocess where the content will be parsed by parse5.
The AST will be walked and the do-tags (e.g. `<do-insert>`) will be replaced.

`const { code } = await svelte.preprocess(yourHtmlCode, doTemplate)` 

